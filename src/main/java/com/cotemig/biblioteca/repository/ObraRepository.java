package com.cotemig.biblioteca.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.biblioteca.model.Obra;


@Repository("obraRepository")
public interface ObraRepository extends JpaRepository<Obra, Integer> {

	
}
