package com.cotemig.biblioteca.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.biblioteca.model.Emprestimo;


@Repository("emprestimoRepository")
public interface EmprestimoRepository extends JpaRepository<Emprestimo, Integer> {
		

}
