package com.cotemig.biblioteca.repository; 



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.biblioteca.model.Cliente;



@Repository("clienteRepository")
public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
	

}

