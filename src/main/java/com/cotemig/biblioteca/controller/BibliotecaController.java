package com.cotemig.biblioteca.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cotemig.biblioteca.model.Cliente;
import com.cotemig.biblioteca.model.Emprestimo;
import com.cotemig.biblioteca.model.Funcionario;
import com.cotemig.biblioteca.model.Obra;
import com.cotemig.biblioteca.service.ClienteService;
import com.cotemig.biblioteca.service.EmprestimoService;
import com.cotemig.biblioteca.service.FuncionarioService;
import com.cotemig.biblioteca.service.ObraService;

@Controller
public class BibliotecaController {

 @Autowired
 private ClienteService clienteService;
 
 @RequestMapping(value = "/insertCliente", method = RequestMethod.GET)
 public ModelAndView insertCliente() {
        return new ModelAndView("insertCliente", "cliente", new Cliente());
 }
 
 @RequestMapping(value = "/insertCliente", method = RequestMethod.POST)
 public String submitInsert(@ModelAttribute("cliente")Cliente cliente, 
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
        
 clienteService.insertCliente(cliente);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/deleteCliente", method = RequestMethod.GET)
 public ModelAndView deleteCliente(Integer id) {
 
 return new ModelAndView("deleteCliente", "cliente", clienteService.getClienteById(id).get());
 }
 
 @RequestMapping(value = "/deleteCliente", method = RequestMethod.POST)
 public String submitDelete(@ModelAttribute("cliente")Cliente cliente,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 clienteService.deleteClienteById(cliente.getId());
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/updateCliente", method = RequestMethod.GET)
 public ModelAndView updateCliente(Integer id) {
 
        return new ModelAndView("updateCliente", "cliente", clienteService.getClienteById(id).get());
 }
 
 @RequestMapping(value = "/updateCliente", method = RequestMethod.POST)
 public String submitUpdate(@ModelAttribute("cliente")Cliente cliente,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 clienteService.updateCliente(cliente);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/readCliente", method = RequestMethod.GET)
 public ModelAndView readCliente() {
        
        ModelAndView mav = new ModelAndView("readCliente");
        mav.addObject("clientes", clienteService.getAllClientes());
        return mav;
 }
 
 @RequestMapping(value = "/cliente", method = RequestMethod.GET)
 public ModelAndView cliente() {
        
        ModelAndView mav = new ModelAndView("cliente");
        mav.addObject("clientes", clienteService.getAllClientes());
        return mav;
 }
 
 @Autowired
 private FuncionarioService funcionarioService;
 
 @RequestMapping(value = "/insertFuncionario", method = RequestMethod.GET)
 public ModelAndView insertFuncionario() {
        return new ModelAndView("insertFuncionario", "funcionario", new Funcionario());
 }
 
 @RequestMapping(value = "/insertFuncionario", method = RequestMethod.POST)
 public String submitInsert(@ModelAttribute("funcionario")Funcionario funcionario, 
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
        
funcionarioService.insertFuncionario(funcionario);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/deleteFuncionario", method = RequestMethod.GET)
 public ModelAndView deleteFuncionario(Integer id) {
 
 return new ModelAndView("deleteFuncionario", "funcionario", funcionarioService.getFuncionarioById(id).get());
 }
 
 @RequestMapping(value = "/deleteFuncionario", method = RequestMethod.POST)
 public String submitDelete(@ModelAttribute("funcionario")Funcionario funcionario,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 funcionarioService.deleteFuncionarioById(funcionario.getId());
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/updateFuncionario", method = RequestMethod.GET)
 public ModelAndView updateFuncionario(Integer id) {
 
        return new ModelAndView("updateFuncionario", "funcionario", funcionarioService.getFuncionarioById(id).get());
 }
 
 @RequestMapping(value = "/updateFuncionario", method = RequestMethod.POST)
 public String submitUpdate(@ModelAttribute("funcionario")Funcionario funcionario,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 funcionarioService.updateFuncionario(funcionario);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/readFuncionario", method = RequestMethod.GET)
 public ModelAndView readFuncionario() {
        
        ModelAndView mav = new ModelAndView("readFuncionario");
        mav.addObject("funcionarios", funcionarioService.getAllFuncionarios());
        return mav;
 }
 
 @RequestMapping(value = "/funcionario", method = RequestMethod.GET)
 public ModelAndView funcionario() {
        
        ModelAndView mav = new ModelAndView("funcionario");
        mav.addObject("funcionarios", funcionarioService.getAllFuncionarios());
        return mav;
 }
 
 @Autowired
 private ObraService obraService;
 
 @RequestMapping(value = "/insertObra", method = RequestMethod.GET)
 public ModelAndView insertObra() {
        return new ModelAndView("insertObra", "obra", new Obra());
 }
 
 @RequestMapping(value = "/insertObra", method = RequestMethod.POST)
 public String submitInsert(@ModelAttribute("obra")Obra obra, 
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
        
obraService.insertObra(obra);
        
        return "redirect:";
 }
 
 
 @RequestMapping(value = "/deleteObra", method = RequestMethod.GET)
 public ModelAndView deleteObra(Integer id) {
 
 return new ModelAndView("deleteObra", "obra", obraService.getObraById(id).get());
 }
 
 @RequestMapping(value = "/deleteObra", method = RequestMethod.POST)
 public String submitDelete(@ModelAttribute("obra")Obra obra,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 obraService.deleteObraById(obra.getId());
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/updateObra", method = RequestMethod.GET)
 public ModelAndView updateObra(Integer id) {
 
        return new ModelAndView("updateObra", "obra", obraService.getObraById(id).get());
 }
 
 @RequestMapping(value = "/updateObra", method = RequestMethod.POST)
 public String submitUpdate(@ModelAttribute("obra")Obra obra,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 obraService.updateObra(obra);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/readObra", method = RequestMethod.GET)
 public ModelAndView readObra() {
        
        ModelAndView mav = new ModelAndView("readObra");
        mav.addObject("obras", obraService.getAllObras());
        return mav;
 }
 
 @RequestMapping(value = "/obra", method = RequestMethod.GET)
 public ModelAndView obra() {
        
        ModelAndView mav = new ModelAndView("obra");
        mav.addObject("obras", obraService.getAllObras());
        return mav;
 }
 
 @Autowired
 private EmprestimoService emprestimoService;
 
 @RequestMapping(value = "/insertEmprestimo", method = RequestMethod.GET)
 public ModelAndView insertEmprestimo() {

        return new ModelAndView("insertEmprestimo", "emprestimo", new Emprestimo());
 }
 
 @RequestMapping(value = "/insertEmprestimo", method = RequestMethod.POST)
 public String submitInsert(@ModelAttribute("emprestimo")Emprestimo emprestimo, 
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
        
emprestimoService.insertEmprestimo(emprestimo);
        
        return "redirect:";
 }
 
 
 @RequestMapping(value = "/deleteEmprestimo", method = RequestMethod.GET)
 public ModelAndView deleteEmprestimo(Integer id) {
 
 return new ModelAndView("deleteEmprestimo", "emprestimo", emprestimoService.getEmprestimoById(id).get());
 }
 
 @RequestMapping(value = "/deleteEmprestimo", method = RequestMethod.POST)
 public String submitDelete(@ModelAttribute("emprestimo")Emprestimo emprestimo,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 emprestimoService.deleteEmprestimoById(emprestimo.getId());
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/updateEmprestimo", method = RequestMethod.GET)
 public ModelAndView updateEmprestimo(Integer id) {
 
        return new ModelAndView("updateEmprestimo", "emprestimo", emprestimoService.getEmprestimoById(id).get());
 }
 
 @RequestMapping(value = "/updateEmprestimo", method = RequestMethod.POST)
 public String submitUpdate(@ModelAttribute("emprestimo")Emprestimo emprestimo,
      BindingResult result, ModelMap model) {
        
 if (result.hasErrors()) {
            return "error";
        }
 
 emprestimoService.updateEmprestimo(emprestimo);
        
        return "redirect:";
 }
 
 @RequestMapping(value = "/readEmprestimo", method = RequestMethod.GET)
 public ModelAndView readEmprestimo() {
        
        ModelAndView mav = new ModelAndView("readEmprestimo");
        mav.addObject("emprestimos", emprestimoService.getAllEmprestimos());
        return mav;
 }
 
 @RequestMapping(value = "/emprestimo", method = RequestMethod.GET)
 public ModelAndView emprestimo() {
        
        ModelAndView mav = new ModelAndView("emprestimo");
        mav.addObject("emprestimos", emprestimoService.getAllEmprestimos());
        return mav;
 }
 
 @RequestMapping(value = "/", method = RequestMethod.GET)
 public ModelAndView index() {
        
        ModelAndView mav = new ModelAndView("index");
        return mav;
 }
}
