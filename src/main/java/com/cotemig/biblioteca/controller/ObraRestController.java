package com.cotemig.biblioteca.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cotemig.biblioteca.model.Obra;
import com.cotemig.biblioteca.service.ObraService;


@RestController
public class ObraRestController {
	
	@Autowired
	private ObraService obraService;
	
	@RequestMapping(value = "/rest/obra/getAll", method = RequestMethod.GET)
	 public List<Obra> getObras() {
	 return obraService.getAllObras();
	 }
	
	@RequestMapping(value = "rest/obra/{id}", method = RequestMethod.GET)
	public Optional<Obra> getObraById(@PathVariable("id") Integer id) {
		return obraService.getObraById(id);
	}
	
	@RequestMapping(value = "/rest/obra/deleteAll", method = RequestMethod.DELETE)
	 public void deleteObras() {
		obraService.deleteAllObras();
	}
	
	@RequestMapping(value = "/rest/obra/delete/{id}", method = RequestMethod.DELETE)
	public void deleteObra(@PathVariable("id") Integer id) {
		obraService.deleteObraById(id);
	}

	@RequestMapping(value = "/rest/obra/update/{id}", method = RequestMethod.POST)
	public void updateObra(@RequestBody Obra obra, @PathVariable("id") Integer id) {
		obraService.updateObraById(id, obra);
	}

	@RequestMapping(value = "/rest/obra/insert", method = RequestMethod.POST)
	public void updateObra(@RequestBody Obra obra) {
		obraService.insertObra(obra);
	}

}
