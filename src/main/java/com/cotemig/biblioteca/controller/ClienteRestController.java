package com.cotemig.biblioteca.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cotemig.biblioteca.model.Cliente;
import com.cotemig.biblioteca.service.ClienteService;


@RestController
public class ClienteRestController {
	
	@Autowired
	private ClienteService clienteService;
	
	@RequestMapping(value = "/rest/cliente/getAll", method = RequestMethod.GET)
	 public List<Cliente> getAlunos() {
	 return clienteService.getAllClientes();
	 }
	
	@RequestMapping(value = "rest/cliente/{id}", method = RequestMethod.GET)
	public Optional<Cliente> getClienteById(@PathVariable("id") Integer id) {
		return clienteService.getClienteById(id);
	}
	
	@RequestMapping(value = "/rest/cliente/deleteAll", method = RequestMethod.DELETE)
	 public void deleteAlunos() {
		clienteService.deleteAllClientes();
	}
	
	@RequestMapping(value = "/rest/cliente/delete/{id}", method = RequestMethod.DELETE)
	public void deleteAluno(@PathVariable("id") Integer id) {
		clienteService.deleteClienteById(id);
	}

	@RequestMapping(value = "/rest/cliente/update/{id}", method = RequestMethod.POST)
	public void updateAluno(@RequestBody Cliente cliente, @PathVariable("id") Integer id) {
		clienteService.updateClienteById(id, cliente);
	}

	@RequestMapping(value = "/rest/cliente/insert", method = RequestMethod.POST)
	public void updateAluno(@RequestBody Cliente cliente) {
		clienteService.insertCliente(cliente);
	}

}
