package com.cotemig.biblioteca.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cotemig.biblioteca.model.Funcionario;
import com.cotemig.biblioteca.service.FuncionarioService;


@RestController
public class FuncionarioRestController {
	
	@Autowired
	private FuncionarioService funcionarioService;
	
	@RequestMapping(value = "/rest/funcionario/getAll", method = RequestMethod.GET)
	 public List<Funcionario> getFuncionarios() {
	 return funcionarioService.getAllFuncionarios();
	 }
	
	@RequestMapping(value = "rest/funcionario/{id}", method = RequestMethod.GET)
	public Optional<Funcionario> getFuncionarioById(@PathVariable("id") Integer id) {
		return funcionarioService.getFuncionarioById(id);
	}
	
	@RequestMapping(value = "/rest/funcionario/deleteAll", method = RequestMethod.DELETE)
	 public void deleteFuncionarios() {
		funcionarioService.deleteAllFuncionarios();
	}
	
	@RequestMapping(value = "/rest/funcionario/delete/{id}", method = RequestMethod.DELETE)
	public void deleteAluno(@PathVariable("id") Integer id) {
		funcionarioService.deleteFuncionarioById(id);
	}

	@RequestMapping(value = "/rest/funcionario/update/{id}", method = RequestMethod.POST)
	public void updateAluno(@RequestBody Funcionario funcionario, @PathVariable("id") Integer id) {
		funcionarioService.updateFuncionarioById(id, funcionario);
	}

	@RequestMapping(value = "/rest/funcionario/insert", method = RequestMethod.POST)
	public void updateAluno(@RequestBody Funcionario funcionario) {
		funcionarioService.insertFuncionario(funcionario);
	}

}
