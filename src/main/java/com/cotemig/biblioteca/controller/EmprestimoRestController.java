package com.cotemig.biblioteca.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cotemig.biblioteca.model.Emprestimo;
import com.cotemig.biblioteca.service.EmprestimoService;

@RestController
public class EmprestimoRestController {
	
	@Autowired
	private EmprestimoService emprestimoService;
	
	@RequestMapping(value = "/rest/emprestimo/getAll", method = RequestMethod.GET)
	 public List<Emprestimo> getEmprestimos() {
	 return emprestimoService.getAllEmprestimos();
	 }
	
	@RequestMapping(value = "rest/emprestimo/{id}", method = RequestMethod.GET)
	public Optional<Emprestimo> getEmprestimoById(@PathVariable("id") Integer id) {
		return emprestimoService.getEmprestimoById(id);
	}
	
	@RequestMapping(value = "/rest/emprestimo/deleteAll", method = RequestMethod.DELETE)
	 public void deleteEmprestimos() {
		emprestimoService.deleteAllEmprestimos();
	}
	
	@RequestMapping(value = "/rest/emprestimo/delete/{id}", method = RequestMethod.DELETE)
	public void deleteEmprestimo(@PathVariable("id") Integer id) {
		emprestimoService.deleteEmprestimoById(id);
	}

	@RequestMapping(value = "/rest/emprestimo/update/{id}", method = RequestMethod.POST)
	public void updateEmprestimo(@RequestBody Emprestimo emprestimo, @PathVariable("id") Integer id) {
		emprestimoService.updateEmprestimoById(id, emprestimo);
	}

	@RequestMapping(value = "/rest/emprestimo/insert", method = RequestMethod.POST)
	public void updateEmprestimo(@RequestBody Emprestimo emprestimo) {
		emprestimoService.insertEmprestimo(emprestimo);
	}

}
