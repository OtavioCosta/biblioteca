package com.cotemig.biblioteca.service;

import java.util.List;
import java.util.Optional;

import com.cotemig.biblioteca.model.Obra;


public interface ObraService {

	Optional<Obra> getObraById(Integer id);
	List<Obra> getAllObras();
	void deleteAllObras();
	void deleteObraById(Integer id);
	void updateObraById(Integer id, Obra obra);
	void updateObra(Obra obra);
	void insertObra(Obra obra);
}
