package com.cotemig.biblioteca.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.biblioteca.model.Emprestimo;
import com.cotemig.biblioteca.repository.EmprestimoRepository;


@Service("emprestimoService")
public class EmprestimoServiceImpl implements EmprestimoService {

	@Autowired
	EmprestimoRepository emprestimoRepository;
	
	@Override
	public Optional<Emprestimo> getEmprestimoById(Integer id) {
		return emprestimoRepository.findById(id);
	}

	@Override
	public List<Emprestimo> getAllEmprestimos() {
		return emprestimoRepository.findAll();
	}

	@Override
	public void deleteAllEmprestimos() {
		emprestimoRepository.deleteAll();
	}

	@Override
	public void deleteEmprestimoById(Integer id) {
		emprestimoRepository.deleteById(id);
	}

	@Override
	public void updateEmprestimoById(Integer id, Emprestimo emprestimo) {
		Optional<Emprestimo> getEmprestimo = getEmprestimoById(id);

		getEmprestimo.get().setcId(emprestimo.getcId());
		getEmprestimo.get().setfId(emprestimo.getfId());
		getEmprestimo.get().setDevolucao(emprestimo.getDevolucao());
		getEmprestimo.get().setStatus(emprestimo.getStatus());
		getEmprestimo.get().setoId(emprestimo.getoId());
		getEmprestimo.get().setMulta(emprestimo.getMulta());
		
		emprestimoRepository.save(emprestimo);
		
	}

	@Override
	public void updateEmprestimo(Emprestimo emprestimo) {
		emprestimoRepository.save(emprestimo);
		
	}

	@Override
	public void insertEmprestimo(Emprestimo emprestimo) {
		emprestimoRepository.save(emprestimo);
	}


}
