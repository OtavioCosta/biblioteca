package com.cotemig.biblioteca.service;

import java.util.List;
import java.util.Optional;

import com.cotemig.biblioteca.model.Funcionario;


public interface FuncionarioService {

	Optional<Funcionario> getFuncionarioById(Integer id);
	List<Funcionario> getAllFuncionarios();
	void deleteAllFuncionarios();
	void deleteFuncionarioById(Integer id);
	void updateFuncionarioById(Integer id, Funcionario funcionario);
	void updateFuncionario(Funcionario funcionario);
	void insertFuncionario(Funcionario funcionario);
}
