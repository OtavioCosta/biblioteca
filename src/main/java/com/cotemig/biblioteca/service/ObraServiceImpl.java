package com.cotemig.biblioteca.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cotemig.biblioteca.model.Obra;
import com.cotemig.biblioteca.repository.ObraRepository;


@Service("ObraService")
public class ObraServiceImpl implements ObraService {

	@Autowired
	ObraRepository obraRepository;
	
	@Override
	public Optional<Obra> getObraById(Integer id) {
		return obraRepository.findById(id);
	}

	@Override
	public List<Obra> getAllObras() {
		return obraRepository.findAll();
	}

	@Override
	public void deleteAllObras() {
		obraRepository.deleteAll();
	}

	@Override
	public void deleteObraById(Integer id) {
		obraRepository.deleteById(id);
	}

	@Override
	public void updateObraById(Integer id, Obra obra) {
		Optional<Obra> getObra = getObraById(id);

		getObra.get().setName(obra.getName());
		getObra.get().setTipo(obra.getTipo());
		
		obraRepository.save(obra);
		
	}

	@Override
	public void updateObra(Obra obra) {
		obraRepository.save(obra);
		
	}

	@Override
	public void insertObra(Obra obra) {
		obraRepository.save(obra);
	}
	


}
