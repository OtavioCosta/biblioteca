package com.cotemig.biblioteca.service;

import java.util.List;
import java.util.Optional;

import com.cotemig.biblioteca.model.Emprestimo;

public interface EmprestimoService {

	Optional<Emprestimo> getEmprestimoById(Integer id);
	List<Emprestimo> getAllEmprestimos();
	void deleteAllEmprestimos();
	void deleteEmprestimoById(Integer id);
	void updateEmprestimoById(Integer id, Emprestimo emprestimo);
	void updateEmprestimo(Emprestimo emprestimo);
	void insertEmprestimo(Emprestimo emprestimo);
}
